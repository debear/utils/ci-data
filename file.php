<?php
// Validate the argument to understand what we are parsing
if (!isset($_GET['file']) || !in_array($_GET['file'], ['geoip', 'browscap'])) {
    http_response_code(404);
    exit(2);
}

// We only want GET or HEAD requests...
if (!in_array($_SERVER['REQUEST_METHOD'], ['GET', 'HEAD'])) {
    http_response_code(405);
    exit(2);
}

// What is the ultimate file we are downlading?
$file = [
    'geoip' => 'GeoLite2-City.mmdb.gz',
    'browscap' => 'resources.tar.gz',
];
$file = $file[$_GET['file']];

// Which folder do we want?
$locations = [
    "{$_GET['repo']}/{$_GET['branch']}",
    "{$_GET['repo']}/master",
    "_global/{$_GET['branch']}",
    "_global/master",
];
foreach ($locations as $s) {
    $d = __DIR__ . "/files/$s/{$_GET['file']}/";
    if (file_exists("$d/$file")) {
        $failed = false;
        break;
    }
    $failed = true;
}
// If none match, we went wrong
if ($failed) {
    http_response_code(500);
    exit(2);
}

// Get the file's MD5
$file_cache = "$d/.cache";
$file_md5 = trim(file_get_contents($file_cache));
header("X-Cache: $file_md5");

// Are we checking to see if the remote is already up to date?
$headers = getallheaders();
if (isset($headers['X-Cache']) && $headers['X-Cache'] !== '') {
    if (!file_exists($file_cache)) {
        http_response_code(500);
        exit(2);
    }
    if ($file_md5 == $headers['X-Cache']) {
        http_response_code(204);
        exit;
    }
}

// If we're in HEAD mode, just return the 200...
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename="'.$file.'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Type: application/octet-stream');
    header('Content-Length: ' . filesize("$d/$file"));
    readfile("$d/$file");
}

<?php
// We only want GET or HEAD requests...
if (!in_array($_SERVER['REQUEST_METHOD'], ['GET', 'POST'])) {
    http_response_code(405);
    exit(2);
}

// Which folder do we want?
$locations = [
    "{$_GET['repo']}/{$_GET['branch']}",
    "{$_GET['repo']}/master",
    "_global/{$_GET['branch']}",
    "_global/master",
];
foreach ($locations as $s) {
    $d = __DIR__ . "/files/$s/database";
    if (file_exists($d)) {
        $failed = false;
        break;
    }
    $failed = true;
}
// If none match, we went wrong
if ($failed) {
    http_response_code(500);
    exit(2);
}

// Compare what we've been given with against the cache
$cache = [];
$cache_raw = trim(file_get_contents("$d/.cache"));
foreach (explode("\n", $cache_raw) as $line) {
    list($f, $m) = explode("\t", $line);
    $cache[$f] = $m;
}

// Passed is already built in the POST contents (which annoyingly converts . to _ in file names)
$passed = [];
foreach ($_POST as $file => $hash) {
    $passed[str_replace('_sql_gz', '.sql.gz', $file)] = $hash;
}

// What are we processing?
$headers = getallheaders();
if (isset($headers['X-HEAD']) && $headers['X-HEAD']) {
    // So what is different then?
    $diff = array_diff($cache, $passed);

    // Has anything been pruned?
    $pruned = array_diff_key($passed, $cache);
    foreach (array_keys($pruned) as $p) {
        $diff[$p] = '_pruned_';
    }

    // If no differences, return empty content status
    if (!sizeof($diff)) {
        http_response_code(204);
        exit;
    }

    // Return the difference
    header('Content-Type: text/plain');
    $ret = [];
    foreach ($diff as $file => $hash) {
        $ret[] = "$file=$hash";
    }
    print join('&', $ret);
    exit;
}

// Processing the files
$tmp_dir = __DIR__ . '/.tmp/' . getmypid();
$tmp_file = 'database.tar.gz';
mkdir($tmp_dir, 0755, true);
copy("$d/.cache", "$tmp_dir/.cache");
foreach (array_keys($passed) as $file) {
    $tmp_file_dir = dirname("$tmp_dir/$file");
    if (!file_exists($tmp_file_dir)) {
        mkdir($tmp_file_dir, 0755, true);
    }
    copy("$d/$file", "$tmp_dir/$file");
}
system("cd $tmp_dir; tar -czf database.tar.gz debearco* \.cache");
header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="'.$tmp_file.'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Type: application/octet-stream');
header('Content-Length: ' . filesize("$tmp_dir/$tmp_file"));
readfile("$tmp_dir/$tmp_file");
system("rm -rf $tmp_dir");
